#!/bin/bash

cd /var/www/content/frameless/
install_latest_composer.sh
php -d memory_limit=-1 composer.phar install --no-interaction
